const express = require("express");
const cors = require('cors');

require("dotenv").config();

const db = require("./app/models");

const {initial} = require("./data");

const mongoose = require('mongoose');

// Khởi tạo Express App
const app = express();

const PORT = process.env.ENV_PORT || 8000;

const productTypeRouter = require("./app/routes/productType.router");
const productRouter = require("./app/routes/product.router");
const customerRouter = require("./app/routes/customer.router");
const orderRouter = require("./app/routes/order.router");
const orderDetailRouter = require("./app/routes/orderDetail.router");

//Cấu hình để sử dụng json
app.use(express.json());

app.use(cors());

app.use("/images", express.static(__dirname + '/images'));

// Khai báo tạm model tại đây
const productTypeSchema = require("./app/models/productType.model");

// Khai báo kết nối mongoDB qua mongoose
mongoose.connect(
    `mongodb://${
        process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`,
    )
    .then(() => {
        console.log("Connect mongoDB Successfully");
        initial();
    })
    .catch((err) => {
        console.log(err);
        process.exit();
    });

// Sử dụng router 
app.use("/api/auth/", require("./app/routes/auth.route"));
app.use("/api/user/", require("./app/routes/user.route"));

app.use("/api/v1/product-types", productTypeRouter);
app.use("/api/v1/products", productRouter);
app.use("/api/v1/customers", customerRouter);
app.use("/api/v1/orders", orderRouter);
app.use("/api/v1/order-details", orderDetailRouter);

app.listen(PORT, () => {
    console.log(`App Listening on port ${PORT}`);
})

module.exports = app;