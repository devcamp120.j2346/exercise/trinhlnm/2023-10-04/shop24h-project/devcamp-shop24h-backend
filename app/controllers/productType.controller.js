const mongoose = require("mongoose");

const productTypeModel = require("../models/productType.model");
const productModel = require("../models/product.model");

const createProductType = async (req, res) => {
    //B1: thu thập dữ liệu
    const { name, description } = req.body;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            message: "Yêu cầu name!"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newPT = {
            name: name,
            description: description,
        }

        const result = await productTypeModel.create(newPT);

        return res.status(201).json({
            message: "Tạo product type thành công",
            data: result
        })
    } catch (error) {
        // Dung cac he thong thu thap loi de thu thap error
        console.log(error.message);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllProductTypes = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await productTypeModel.find();

        return res.status(200).json({
            message: "Lấy danh sách product type thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getFourTypesFourProducts = async (req, res) => {
    try {
        const fourTypes = await productTypeModel.find().sort({ createdAt: -1 }).limit(4);

        const result = await Promise.all(fourTypes.map( async (e) => {
            var fourProducts = await productModel.find({ type: { "$in": e._id } }).sort({ createdAt: -1 }).limit(4).populate("type");
            return {...e._doc, fourProducts};
        }));

        return res.status(200).json({
            message: "Lấy danh sách 4 types 4 products thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getProductTypeById = async (req, res) => {
    // B1: Thu thap du lieu
    const productTypeId = req.params.productTypeId;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            message: "Product type ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await productTypeModel.findById(productTypeId);

        if(result) {
            return res.status(200).json({
                message: "Lấy thông tin product type thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin product type"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateProductTypeById = async (req, res) => {
    // B1: Thu thap du lieu
    const productTypeId = req.params.productTypeId;
    const { name, description } = req.body;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            message: "Product type ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateProductType = {};
        if(name) {
            newUpdateProductType.name = name;
        }
        if(description) {
            newUpdateProductType.description = description;
        }

        const result = await productTypeModel.findByIdAndUpdate(productTypeId, newUpdateProductType);
        const finalResult = await productTypeModel.findById(productTypeId);

        if(result) {
            return res.status(200).json({
                message: "Update thông tin product type thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin product type"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}   

const deleteProductTypeById = async (req, res) => {
    // B1: Thu thap du lieu
    const productTypeId = req.params.productTypeId;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            message: "Product type ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await productTypeModel.findByIdAndRemove(productTypeId);

        if(result) {
            return res.status(200).json({
                message: "Xóa thông tin product type thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin product type"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createProductType,
    getAllProductTypes,
    getProductTypeById,
    getFourTypesFourProducts,
    updateProductTypeById,
    deleteProductTypeById
};