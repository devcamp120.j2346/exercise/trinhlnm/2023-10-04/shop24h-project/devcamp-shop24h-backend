const mongoose = require("mongoose");

const customerModel = require("../models/customer.model");
const orderModel = require("../models/order.model");
const orderDetailModel = require("../models/orderDetail.model");

const createCustomer = async (req, res) => {
    //B1: thu thập dữ liệu
    const {
        fullName,
        phone,
        email,
        address,
        city,
        country,
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!fullName) {
        return res.status(400).json({
            message: "Yêu cầu fullName!"
        })
    }

    if (!phone) {
        return res.status(400).json({
            message: "Yêu cầu phone"
        })
    }
    if (validatePhone(phone) == false) {
        return res.status(400).json({
            message: "Phone không đúng định dạng!"
        })
    }

    if (!email) {
        return res.status(400).json({
            message: "Yêu cầu email"
        })
    }
    if (validateEmail(email) == false) {
        return res.status(400).json({
            message: "Email không đúng định dạng!"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newCustomer = {
            fullName: fullName,
            phone: phone,
            email: email,
            address: address,
            city: city,
            country: country,
        }

        const result = await customerModel.create(newCustomer);

        return res.status(201).json({
            message: "Tạo Customer thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllCustomers = async (req, res) => {
    // B1: Thu thap du lieu
    let limit = req.query.limit;
    let page = req.query.page;
    let fullName = req.query.fullName;
    let city = req.query.city;
    let country = req.query.country;
    let isDel = req.query.isDel;

    // B2: Validate du lieu
    // B3: Xu ly du lieu
    if (limit && page) {
        try {
            let criteria = [];

            if (isDel) {
                criteria.push({ delete: true });
            } else {
                criteria.push({ delete: false });
            }

            if (fullName && fullName.length > 0) {
                criteria.push({ fullName: fullName });
            }

            if (city && city.length > 0) {
                criteria.push({ city: city });
            }

            if (country && country.length > 0) {
                criteria.push({ country: country });
            }

            criteria = { $and: criteria };

            const totalCustomers = await customerModel.count(criteria);

            const resultPag = await customerModel.find(criteria)
                .sort({ createdAt: -1 })
                .skip(limit * (page - 1))
                .limit(limit)
                .populate("orders");

            const resultOrder = resultPag.map((e) => {
                var vOrders = e.orders;
                var vOrderNotDels = vOrders.filter((order) => {
                    return order.delete == false;
                });
                e.orders = vOrderNotDels;
                return e;
            })

            return res.status(200).json({
                message: "Lấy danh sách customer phân trang thành công",
                total: totalCustomers,
                data: resultOrder
            })
        } catch (error) {
            return res.status(500).json({
                message: "Đã có lỗi!"
            })
        }
    } else {
        try {
            const result = await customerModel.find();

            return res.status(200).json({
                message: "Lấy danh sách Customer thành công",
                data: result
            })
        } catch (error) {
            return res.status(500).json({
                message: "Đã có lỗi!"
            })
        }
    }
}

const getCustomerById = async (req, res) => {
    // B1: Thu thap du lieu
    const customerId = req.params.customerId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Customer ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await customerModel.findById(customerId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin Customer thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin Customer"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateCustomerById = async (req, res) => {
    // B1: Thu thap du lieu
    const customerId = req.params.customerId;

    const {
        fullName,
        phone,
        email,
        address,
        city,
        country,
        isDel
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Customer ID không hợp lệ"
        })
    }

    if (phone && validatePhone(phone) == false) {
        return res.status(400).json({
            message: "Phone không đúng định dạng!"
        })
    }

    if (email && validateEmail(email) == false) {
        return res.status(400).json({
            message: "Email không đúng định dạng!"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateCustomer = {};
        if (fullName) {
            newUpdateCustomer.fullName = fullName;
        }
        if (phone) {
            newUpdateCustomer.phone = phone;
        }
        if (email) {
            newUpdateCustomer.email = email;
        }
        if (address) {
            newUpdateCustomer.address = address;
        }
        if (city) {
            newUpdateCustomer.city = city;
        }
        if (country) {
            newUpdateCustomer.country = country;
        }

        const customerDel = await customerModel.findById(customerId);
        if (isDel == false) {
            newUpdateCustomer.delete = false;
            const orderDelsResult = await Promise.all(customerDel.orders.map(async (e) => {
                await orderModel.findByIdAndUpdate(e, {delete: false});
            }));
        }
        if (isDel == true) {
            newUpdateCustomer.delete = true;
            const orderDelsResult = await Promise.all(customerDel.orders.map(async (e) => {
                await orderModel.findByIdAndUpdate(e, {delete: true});
            }));
        }

        const result = await customerModel.findByIdAndUpdate(customerId, newUpdateCustomer);
        const finalResult = await customerModel.findById(customerId);

        if (result) {
            return res.status(200).json({
                message: "Update thông tin Customer thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin Customer"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteCustomerById = async (req, res) => {
    // B1: Thu thap du lieu
    const customerId = req.params.customerId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "Customer ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const customerDel = await customerModel.findById(customerId);

        var orderDetailDels = [];

        for(let bI = 0; bI < customerDel.orders.length; bI++) {
            const orderDel = await orderModel.findById(customerDel.orders[bI]);
            orderDetailDels = [...orderDetailDels, ...orderDel.orderDetails];
        }

        const orderDetailsDelsResult = await Promise.all(orderDetailDels.map(async (e) => {
            await orderDetailModel.findByIdAndRemove(e);
        }));

        const orderDelsResult = await Promise.all(customerDel.orders.map(async (e) => {
            await orderModel.findByIdAndRemove(e);
        }));

        const result = await customerModel.findByIdAndRemove(customerId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin Customer thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin Customer"
            })
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

//hàm kiểm tra số điện thoại
function validatePhone(paramPhone) {
    // Số điện thoại phải nhập là số, có dấu + hoặc số 0 ở đầu; độ dài từ 10 đến 12 ký tự

    if (isNaN(paramPhone)) {
        return false;
    }

    if (paramPhone.charAt(0) != "0" && paramPhone.charAt(0) != "+") {
        return false;
    }

    if (paramPhone.length < 10 || paramPhone.length > 12) {
        return false;
    }

    return true;
}

// hàm kiểm tra email đúng định dạng ko?
function validateEmail(paramEmail) {
    var vValidRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail.match(vValidRegex)) {
        return true;
    } else {
        return false;
    }
}

module.exports = {
    createCustomer,
    getAllCustomers,
    getCustomerById,
    updateCustomerById,
    deleteCustomerById
};