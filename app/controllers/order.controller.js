const mongoose = require("mongoose");

const orderModel = require("../models/order.model");
const customerModel = require("../models/customer.model");
const orderDetailModel = require("../models/orderDetail.model");
const userModel = require("../models/user.model");
const productModel = require("../models/product.model");

const createOrderWithCustomer = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        fullName,
        phone,
        email,
        address,
        city,
        country,
        note,
        orderDetails,
        cost,
        userId
    } = req.body;

    //B2: kiểm tra dữ liệu
    var vAmountChecks = [];
    const amountInfos = await Promise.all(orderDetails.map(async (e) => {
        const result = await productModel.findById(e.productId);
        return { ...e, store: result ? result.amount : 0 };
    }));

    for (let bI = 0; bI < amountInfos.length; bI++) {
        if (amountInfos[bI].quantity > amountInfos[bI].store) {
            vAmountChecks.push(amountInfos[bI]);
        }
    }

    if (vAmountChecks.length != 0) {
        return res.status(400).json({
            message: "Số lượng không đủ!",
            amountCheck: vAmountChecks
        })
    }

    // B3: Xu ly du lieu
    try {
        const customer = await customerModel.findOne({
            phone: phone
        });

        if (customer) {
            if (fullName != customer.fullName) {
                customer.fullName = fullName;
            }
            if (email != customer.email) {
                customer.email = email;
            }
            if (address != customer.address) {
                customer.address = address;
            }
            if (city != customer.city) {
                customer.city = city;
            }
            if (country != customer.email) {
                customer.country = country;
            }

            const customerUpdate = await customerModel.findByIdAndUpdate(customer._id, customer);

            //tạo order mới
            const orderDetailsResult = await Promise.all(orderDetails.map(async (e) => {
                const resultE = await orderDetailModel.create({
                    productName: e.productName,
                    productId: e.productId,
                    productPrice: e.productPrice,
                    quantity: e.quantity,
                });
                return resultE._id;
            }));

            var newOrder = {
                customer: customer._id,
                orderDate: Date.now(),
                note: note,
                orderDetails: orderDetailsResult,
                cost: cost,
            }

            const orderResult = await orderModel.create(newOrder);

            const customerOrder = await customerModel.findByIdAndUpdate(customer._id, {
                $push: { orders: orderResult._id }
            })
            const finalResult = await customerModel.findById(customer._id);

            //cập nhật lại số lượng sản phẩm
            for(let bI = 0; bI < amountInfos.length; bI++) {
                var item = amountInfos[bI];
                await productModel.findByIdAndUpdate(item.productId, {amount: item.store - item.quantity});
            }

            return res.status(201).json({
                message: "Tạo order kèm customer thành công",
                data: finalResult
            })
        } else {
            var newCustomer = {
                fullName: fullName,
                phone: phone,
                email: email,
                address: address,
                city: city,
                country: country,
            }

            const customerCreated = await customerModel.create(newCustomer);

            //tạo order mới
            const orderDetailsResult2 = await Promise.all(orderDetails.map(async (e) => {
                const resultE2 = await orderDetailModel.create({
                    productName: e.productName,
                    productId: e.productId,
                    productPrice: e.productPrice,
                    quantity: e.quantity,
                });
                return resultE2._id;
            }));

            var newOrder2 = {
                customer: customerCreated._id,
                orderDate: Date.now(),
                note: note,
                orderDetails: orderDetailsResult2,
                cost: cost,
            }

            const orderResult2 = await orderModel.create(newOrder2);

            const customerOrder = await customerModel.findByIdAndUpdate(customerCreated._id, {
                $push: { orders: orderResult2._id }
            })
            const finalResult2 = await customerModel.findById(customerCreated._id);

            //gán customer cho user
            const user = await userModel.findOne({
                _id: userId
            });
            if (!user.customer) {
                await userModel.findByIdAndUpdate(userId, { customer: customerCreated._id });
            }

            return res.status(201).json({
                message: "Tạo order kèm customer thành công",
                data: finalResult2
            })
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const createOrderOfCustomer = async (req, res) => {
    //B1: thu thập dữ liệu
    const customerId = req.params.customerId;
    const {
        orderDate,
        shippedDate,
        note,
        cost,
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "customer ID không hợp lệ"
        })
    }

    if (isNaN(cost) || cost < 0) {
        return res.status(400).json({
            message: "Cost không hợp lệ!"
        })
    }

    try {
        var orderDateType = dateStringToMilliseconds(orderDate);
        var shippedDateType = dateStringToMilliseconds(shippedDate);
        // B3: Xu ly du lieu
        var newOrder = {
            orderDate: orderDateType,
            shippedDate: shippedDateType,
            note: note,
            cost: cost,
        }

        const result = await orderModel.create(newOrder);

        const updatedCustomer = await customerModel.findByIdAndUpdate(customerId, {
            $push: { orders: result._id }
        })

        return res.status(201).json({
            message: "Tạo order thành công",
            customer: updatedCustomer,
            data: result
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllOrders = async (req, res) => {
    // B1: Thu thap du lieu
    let limit = req.query.limit;
    let page = req.query.page;
    let customer = req.query.customer;
    let product = req.query.product;
    let sdToggle = req.query.sdToggle;
    let sdMin = req.query.sdMin;
    let sdMax = req.query.sdMax;
    let odMin = req.query.odMin;
    let odMax = req.query.odMax;
    let costMin = req.query.costMin;
    let costMax = req.query.costMax;
    let isDel = req.query.isDel;

    // B2: Validate du lieu
    // B3: Xu ly du lieu
    if (limit && page) {
        try {
            let criteria = [];
            let sortField = { createdAt: -1 };

            if (isDel) {
                criteria.push({ delete: true });
            } else {
                criteria.push({ delete: false });
            }

            if (customer) {
                criteria.push({ customer: customer });
            }

            if (product) {
                const orderDetails = await orderDetailModel.find({ productId: product });
                var productCriteria = [];
                for (let bI = 0; bI < orderDetails.length; bI++) {
                    productCriteria.push(orderDetails[bI]._id)
                }
                criteria.push({ orderDetails: { "$in": productCriteria } });
            }

            var sdMin1dayLess = new Date(sdMin).getTime() - 86400000;
            if (sdMin && sdMax) {
                sortField = { shippedDate: -1 };
                criteria.push({ shippedDate: { "$gte": sdMin1dayLess, "$lte": sdMax } });
            } else if (sdMin) {
                sortField = { shippedDate: -1 };
                criteria.push({ shippedDate: { "$gte": sdMin1dayLess } });
            } else if (sdMax) {
                sortField = { shippedDate: -1 };
                criteria.push({ shippedDate: { "$lte": sdMax } });
            } else if (sdToggle) {
                criteria.push({ shippedDate: null });
            }

            var odMin1dayLess = new Date(odMin).getTime() - 86400000;
            var odMax1dayLess = new Date(odMax).getTime() + 86400000;
            if (odMin && odMax) {
                sortField = { orderDate: -1 };
                criteria.push({ orderDate: { "$gte": odMin1dayLess, "$lte": odMax1dayLess } });
            } else if (odMin) {
                sortField = { orderDate: -1 };
                criteria.push({ orderDate: { "$gte": odMin1dayLess } });
            } else if (odMax) {
                sortField = { orderDate: -1 };
                criteria.push({ orderDate: { "$lte": odMax1dayLess } });
            }

            if (costMin && costMax) {
                sortField = { cost: -1 };
                criteria.push({ cost: { "$gte": costMin, "$lte": costMax } });
            } else if (costMin) {
                sortField = { cost: -1 };
                criteria.push({ cost: { "$gte": costMin } });
            } else if (costMax) {
                sortField = { cost: -1 };
                criteria.push({ cost: { "$lte": costMax } });
            }

            //criteria = criteria.length > 0 ? { $and: criteria } : {};
            criteria = { $and: criteria };

            const totalOrders = await orderModel.count(criteria);

            const result = await orderModel.find(criteria)
                .sort(sortField)
                .skip(limit * (page - 1))
                .limit(limit)
                .populate("customer").populate("orderDetails");

            return res.status(200).json({
                message: "Lấy danh sách order phân trang thành công",
                total: totalOrders,
                data: result
            })
        } catch (error) {
            console.log(error);
            return res.status(500).json({
                message: "Đã có lỗi!"
            })
        }
    } else {
        try {
            const result = await orderModel.find().populate("customer").populate("orderDetails");

            return res.status(200).json({
                message: "Lấy danh sách order thành công",
                data: result
            })
        } catch (error) {
            console.log(error);
            return res.status(500).json({
                message: "Đã có lỗi!"
            })
        }
    }
}

const getAllOrderOfCustomer = async (req, res) => {
    //B1: thu thập dữ liệu
    const customerId = req.params.customerId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            message: "customer ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const customerInfo = await customerModel.findById(customerId).populate("orders");

        return res.status(200).json({
            message: "Lấy danh sách order của customer thành công",
            data: customerInfo.orders
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getOrderById = async (req, res) => {
    // B1: Thu thap du lieu
    const orderId = req.params.orderId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "order ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await orderModel.findById(orderId).populate("customer").populate("orderDetails");

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin order thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateOrderById = async (req, res) => {
    // B1: Thu thap du lieu
    const orderId = req.params.orderId;

    const {
        shippedDate,
        isDel
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "order ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateOrder = {};
        if (shippedDate) {
            var shippedDateType = dateStringToMilliseconds(shippedDate);
            newUpdateOrder.shippedDate = shippedDateType;
        }

        const order = await orderModel.findById(orderId).populate("customer");
        if (isDel == false) {
            newUpdateOrder.delete = false;
            if(order.customer.delete == true) {
                await customerModel.findByIdAndUpdate(order.customer._id, {delete: false})
            }
        }
        if (isDel == true) {
            newUpdateOrder.delete = true;
        }

        const result = await orderModel.findByIdAndUpdate(orderId, newUpdateOrder);
        const finalResult = await orderModel.findById(orderId);

        if (result) {
            return res.status(200).json({
                message: "Update thông tin order thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteOrderById = async (req, res) => {
    // B1: Thu thap du lieu
    const orderId = req.params.orderId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "order ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const orderDetails = await orderModel.findById(orderId).populate("orderDetails");

        const updatedCustomer = await customerModel.findByIdAndUpdate(orderDetails.customer, {
            $pull: { orders: orderId }
        })

        const orderDetailsDel = await Promise.all(orderDetails.orderDetails.map(async (e) => {
            await orderDetailModel.findByIdAndRemove(e._id);
        }));

        const result = await orderModel.findByIdAndRemove(orderId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin order thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

//hàm kiểm tra số điện thoại
function validatePhone(paramPhone) {
    // Số điện thoại phải nhập là số, có dấu + hoặc số 0 ở đầu; độ dài từ 10 đến 12 ký tự

    if (isNaN(paramPhone)) {
        return false;
    }

    if (paramPhone.charAt(0) != "0" && paramPhone.charAt(0) != "+") {
        return false;
    }

    if (paramPhone.length < 10 || paramPhone.length > 12) {
        return false;
    }

    return true;
}

// hàm kiểm tra email đúng định dạng ko?
function validateEmail(paramEmail) {
    var vValidRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail.match(vValidRegex)) {
        return true;
    } else {
        return false;
    }
}

//hàm đổi ngày thành milliseconds
function dateStringToMilliseconds(dateString) {
    var dateTokens = dateString.split("/");
    var date = new Date(dateTokens[2], dateTokens[1] - 1, dateTokens[0]);
    return date.getTime();
}

module.exports = {
    createOrderOfCustomer,
    getAllOrders,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrderById,
    deleteOrderById,
    createOrderWithCustomer
};