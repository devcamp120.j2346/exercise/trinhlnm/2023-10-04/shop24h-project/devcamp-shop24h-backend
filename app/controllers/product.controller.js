const mongoose = require("mongoose");
const fs = require('fs');
const productModel = require("../models/product.model");
const orderDetailModel = require("../models/orderDetail.model");

const createProduct = async (req, res) => {
    //B1: thu thập dữ liệu
    const {
        name,
        description,
        type,
        buyPrice,
        promotionPrice,
        amount,
    } = req.body;
    const imageUrl = req.file.filename;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            message: "Yêu cầu name!"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(type)) {
        return res.status(400).json({
            message: "product type không hợp lệ"
        })
    }

    if (!imageUrl) {
        return res.status(400).json({
            message: "Yêu cầu imageUrl!"
        })
    }

    if (isNaN(buyPrice) || buyPrice < 0) {
        return res.status(400).json({
            message: "Buy price không hợp lệ!"
        })
    }

    if (isNaN(promotionPrice) || promotionPrice < 0 || promotionPrice > buyPrice) {
        return res.status(400).json({
            message: "Promotion price không hợp lệ!"
        })
    }

    if (!Number.isInteger(Number(amount)) || Number(amount) < 0) {
        return res.status(400).json({
            message: "Amount không hợp lệ!"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newProduct = {
            name: name,
            description: description,
            type: type,
            imageUrl: "http://localhost:8080/images/" + imageUrl,
            buyPrice: buyPrice,
            promotionPrice: promotionPrice,
            amount: amount
        }

        const result = await productModel.create(newProduct);

        return res.status(201).json({
            message: "Tạo product thành công",
            data: result
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllProducts = async (req, res) => {
    // B1: Thu thap du lieu
    let limit = req.query.limit;
    let page = req.query.page;
    let name = req.query.name;
    let minPrice = req.query.minPrice;
    let maxPrice = req.query.maxPrice;
    let typeId = req.query.typeId;
    let id = req.query.id;
    let productType = req.query.productType;
    let isDel = req.query.isDel;

    // B2: Validate du lieu
    limit = parseInt(limit);
    if (limit && (!Number.isInteger(limit) || limit < 0)) {
        return res.status(400).json({
            message: "Limit không hợp lệ!"
        })
    }

    page = parseInt(page);
    if (page && (!Number.isInteger(page) || page < 0)) {
        return res.status(400).json({
            message: "Page không hợp lệ!"
        })
    }

    // B3: Xu ly du lieu
    if (limit && page) {
        try {

            let criteria = [];

            if (isDel) {
                criteria.push({ delete: true });
            } else {
                criteria.push({ delete: false });
            }

            if (name && name.length > 0) {
                criteria.push({ name: name });
            }

            if (minPrice && maxPrice) {
                criteria.push({ buyPrice: { "$gte": minPrice, "$lte": maxPrice } });
            } else if (minPrice) {
                criteria.push({ buyPrice: { "$gte": minPrice } });
            } else if (maxPrice) {
                criteria.push({ buyPrice: { "$lte": maxPrice } });
            }

            if (productType) {
                criteria.push({ type: { "$in": productType } });
            }

            criteria = { $and: criteria };

            const totalProduct = await productModel.count(criteria);

            const resultPag = await productModel.find(criteria)
                .sort({ createdAt: -1 })
                .skip(limit * (page - 1))
                .limit(limit).populate("type");

            return res.status(200).json({
                message: "Lấy danh sách product phân trang thành công",
                total: totalProduct,
                data: resultPag
            })
        } catch (error) {
            return res.status(500).json({
                message: "Đã có lỗi!"
            })
        }
    }

    try {
        condition2 = [];

        if (id) {
            condition2.push({ _id: { "$ne": id } });
        }

        if (typeId) {
            condition2.push({ type: typeId });
        }

        condition2 = condition2.length > 0 ? { $and: condition2 } : {};

        const result = await productModel.find(condition2).sort({ createdAt: -1 }).limit(limit).populate("type");

        return res.status(200).json({
            message: "Lấy danh sách product thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getProductById = async (req, res) => {
    // B1: Thu thap du lieu
    const productId = req.params.productId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: "Product ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await productModel.findById(productId).populate("type");

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin product thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin product"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateProductById = async (req, res) => {
    // B1: Thu thap du lieu
    const productId = req.params.productId;

    const {
        name,
        description,
        type,
        buyPrice,
        promotionPrice,
        amount,
        isDel
    } = req.body;

    const currentProduct = await productModel.findById(productId);

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: "Product ID không hợp lệ"
        })
    }

    if (type && (typeof type != "string" || !mongoose.Types.ObjectId.isValid(type))) {
        return res.status(400).json({
            message: "product type không hợp lệ"
        })
    }

    if (buyPrice && (isNaN(buyPrice) || buyPrice < 0)) {
        return res.status(400).json({
            message: "Buy price không hợp lệ!"
        })
    }

    if (promotionPrice && (isNaN(promotionPrice) || promotionPrice < 0 || promotionPrice > buyPrice)) {
        return res.status(400).json({
            message: "Promotion price không hợp lệ!"
        })
    }

    if (amount && (!Number.isInteger(Number(amount)) || Number(amount) < 0)) {
        return res.status(400).json({
            message: "Amount không hợp lệ!"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateProduct = {};
        if (name) {
            newUpdateProduct.name = name;
        }
        if (description) {
            newUpdateProduct.description = description;
        }
        if (type) {
            newUpdateProduct.type = type;
        }
        if (buyPrice) {
            newUpdateProduct.buyPrice = buyPrice;
        }
        if (promotionPrice) {
            newUpdateProduct.promotionPrice = promotionPrice;
        }
        if (amount) {
            newUpdateProduct.amount = amount;
        }

        if(req.file) {
            const imageUrl = req.file.filename;
            newUpdateProduct.imageUrl = "http://localhost:8080/images/" + imageUrl;
            
            var oldImageUrl = currentProduct.imageUrl;
            var oldImagePath = oldImageUrl.substring("https://localhost:8080/".length - 1);
            fs.unlinkSync(oldImagePath);
        }

        if (isDel == "false") {
            console.log(typeof isDel)
            newUpdateProduct.delete = false;
        }

        if (isDel == "true") {
            console.log(typeof isDel)
            newUpdateProduct.delete = true;
        }

        const result = await productModel.findByIdAndUpdate(productId, newUpdateProduct);
        const finalResult = await productModel.findById(productId);

        if (result) {
            return res.status(200).json({
                message: "Update thông tin product thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin product"
            })
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteProductById = async (req, res) => {
    // B1: Thu thap du lieu
    const productId = req.params.productId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            message: "Product ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const currentProduct = await productModel.findById(productId);
        var oldImageUrl = currentProduct.imageUrl;
        var oldImagePath = oldImageUrl.substring("https://localhost:8080/".length - 1);
        fs.unlinkSync(oldImagePath);

        const result = await productModel.findByIdAndRemove(productId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin product thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin product"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createProduct,
    getAllProducts,
    getProductById,
    updateProductById,
    deleteProductById
};