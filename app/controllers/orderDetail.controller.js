const mongoose = require("mongoose");

const orderModel = require("../models/order.model");
const orderDetailModel = require("../models/orderDetail.model");

const createOrderDetailOfOrder = async (req, res) => {
    //B1: thu thập dữ liệu
    const orderId = req.params.orderId;
    const {
        product,
        quantity
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "order ID không hợp lệ"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(product)) {
        return res.status(400).json({
            message: "product ID không hợp lệ"
        })
    }

    if (!Number.isInteger(quantity) || quantity <= 0) {
        return res.status(400).json({
            message: "quantity không hợp lệ!"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newOrderDetail = {
            product: product,
            quantity: quantity
        }

        const result = await orderDetailModel.create(newOrderDetail);

        const updatedOrder = await orderModel.findByIdAndUpdate(orderId, {
            $push: { orderDetails: result._id }
        })

        return res.status(201).json({
            message: "Tạo order detail thành công",
            order: updatedOrder,
            data: result
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllOrderDetailOfOrder = async (req, res) => {
    //B1: thu thập dữ liệu
    const orderId = req.params.orderId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            message: "order ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const orderInfo = await orderModel.findById(orderId).populate({ path: 'orderDetails', model: orderDetailModel });

        return res.status(200).json({
            message: "Lấy danh sách order detail của order thành công",
            data: orderInfo.orderDetails
        })
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getOrderDetailById = async (req, res) => {
    // B1: Thu thap du lieu
    const orderDetailId = req.params.orderDetailId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            message: "order detail ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await orderDetailModel.findById(orderDetailId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin order detail thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin order detail"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateOrderDetailById = async (req, res) => {
    // B1: Thu thap du lieu
    const orderDetailId = req.params.orderDetailId;

    const {
        product,
        quantity
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            message: "order detail ID không hợp lệ"
        })
    }

    if (product && !mongoose.Types.ObjectId.isValid(product)) {
        return res.status(400).json({
            message: "product ID không hợp lệ"
        })
    }

    if (quantity && (!Number.isInteger(quantity) || quantity <= 0)) {
        return res.status(400).json({
            message: "quantity không hợp lệ!"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateOrderDetail = {};
        if (product) {
            newUpdateOrderDetail.product = product;
        }
        if (quantity) {
            newUpdateOrderDetail.quantity = quantity;
        }

        const result = await orderDetailModel.findByIdAndUpdate(orderDetailId, newUpdateOrderDetail);
        const finalResult = await orderDetailModel.findById(orderDetailId);

        if (result) {
            return res.status(200).json({
                message: "Update thông tin order detail thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin order detail"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteOrderDetailById = async (req, res) => {
    // B1: Thu thap du lieu
    const orderDetailId = req.params.orderDetailId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            message: "order detail ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await orderDetailModel.findByIdAndRemove(orderDetailId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin order detail thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin order detail"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

module.exports = {
    createOrderDetailOfOrder,
    getAllOrderDetailOfOrder,
    getOrderDetailById,
    updateOrderDetailById,
    deleteOrderDetailById
};