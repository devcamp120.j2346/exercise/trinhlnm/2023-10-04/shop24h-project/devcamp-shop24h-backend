const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const orderDetailSchema = new Schema({
    productName: {
        type: String,
        required: true
    },
    productId: {
        type: mongoose.Types.ObjectId,
        ref: "Product"
    },
    productPrice: {
        type: Number,
        required: true
    },
    quantity: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("OrderDetail", orderDetailSchema);
