const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const orderSchema = new Schema({
    customer: {
        type: mongoose.Types.ObjectId,
        ref: "Customer"
    },
    orderDate: {
        type: Date,
        default: Date.now()
    },
    shippedDate: {
        type: Date
    },
    note: {
        type: String
    },
    orderDetails: [
        {
            type: mongoose.Types.ObjectId,
            ref: "OrderDetail"
        }
    ],
    cost: {
        type: Number,
        default: 0
    },
    delete: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Order", orderSchema);
