const mongoose = require("mongoose");

const userModel = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    roles: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role"
    }],
    customer: {
        type: mongoose.Types.ObjectId,
        ref: "Customer"
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("User", userModel);