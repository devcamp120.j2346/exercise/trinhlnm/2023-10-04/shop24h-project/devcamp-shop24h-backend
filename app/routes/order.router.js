const express = require("express");

const router = express.Router();

const userMiddleWare = require("../middlewares/user.middleware");

const {
    createOrderOfCustomer,
    getAllOrders,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrderById,
    deleteOrderById,
    createOrderWithCustomer
} = require("../controllers/order.controller");

router.get("/", getAllOrders);

router.post("/order-with-customer", [userMiddleWare.verifyToken], createOrderWithCustomer);

router.get("/:customerId/customer", getAllOrderOfCustomer);

router.post("/:customerId", [userMiddleWare.verifyToken, userMiddleWare.checkUser], createOrderOfCustomer);

router.get("/:orderId", getOrderById);

router.put("/:orderId", [userMiddleWare.verifyToken, userMiddleWare.checkUser], updateOrderById);

router.delete("/:orderId", [userMiddleWare.verifyToken, userMiddleWare.checkUser], deleteOrderById);

module.exports = router;