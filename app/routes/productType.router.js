const express = require("express");

const router = express.Router();

const {
    createProductType,
    getAllProductTypes,
    getProductTypeById,
    getFourTypesFourProducts,
    updateProductTypeById,
    deleteProductTypeById
} = require("../controllers/productType.controller");

router.post("/", createProductType);

router.get("/", getAllProductTypes);

router.get("/fourTypesFourProducts", getFourTypesFourProducts);

router.get("/:productTypeId", getProductTypeById)

router.put("/:productTypeId", updateProductTypeById)

router.delete("/:productTypeId", deleteProductTypeById)

module.exports = router;