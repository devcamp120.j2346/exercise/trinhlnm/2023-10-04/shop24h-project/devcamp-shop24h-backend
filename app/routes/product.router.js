const express = require("express");
const userMiddleWare = require("../middlewares/user.middleware");

const multer = require('multer');
const { v4: uuidv4 } = require('uuid');
let path = require('path');

const router = express.Router();

const {
    createProduct,
    getAllProducts,
    getProductById,
    updateProductById,
    deleteProductById
} = require("../controllers/product.controller");

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'images');
    },
    filename: function(req, file, cb) {   
        cb(null, uuidv4() + '-' + Date.now() + path.extname(file.originalname));
    }
});

const fileFilter = (req, file, cb) => {
    const allowedFileTypes = ['image/jpeg', 'image/jpg', 'image/png'];
    if(allowedFileTypes.includes(file.mimetype)) {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

let upload = multer({ storage, fileFilter });
router.post("/", [userMiddleWare.verifyToken, userMiddleWare.checkUser, upload.single('image')], createProduct);

router.get("/", getAllProducts);

router.get("/:productId", getProductById)

router.put("/:productId", [userMiddleWare.verifyToken, userMiddleWare.checkUser, upload.single('image')], updateProductById)

router.delete("/:productId", [userMiddleWare.verifyToken, userMiddleWare.checkUser], deleteProductById)

module.exports = router;