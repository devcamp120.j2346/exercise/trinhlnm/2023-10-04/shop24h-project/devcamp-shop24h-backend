const express = require("express");

const router = express.Router();

const {
    createOrderDetailOfOrder,
    getAllOrderDetailOfOrder,
    getOrderDetailById,
    updateOrderDetailById,
    deleteOrderDetailById
} = require("../controllers/orderDetail.controller");

router.get("/:orderId/Order", getAllOrderDetailOfOrder);

router.post("/:orderId", createOrderDetailOfOrder);

router.get("/:orderDetailId", getOrderDetailById);

router.put("/:orderDetailId", updateOrderDetailById);

router.delete("/:orderDetailId", deleteOrderDetailById);

module.exports = router;