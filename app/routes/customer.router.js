const express = require("express");

const router = express.Router();

const userMiddleWare = require("../middlewares/user.middleware");

const {
    createCustomer,
    getAllCustomers,
    getCustomerById,
    updateCustomerById,
    deleteCustomerById
} = require("../controllers/customer.controller");

router.get("/", getAllCustomers);

router.post("/", [userMiddleWare.verifyToken, userMiddleWare.checkUser], createCustomer);

router.get("/:customerId", getCustomerById);

router.put("/:customerId", [userMiddleWare.verifyToken, userMiddleWare.checkUser], updateCustomerById);

router.delete("/:customerId", [userMiddleWare.verifyToken, userMiddleWare.checkUser], deleteCustomerById);

module.exports = router;