const db = require("../models");
const {v4: uuidv4} = require("uuid");

const createToken = async (user) => {
    let expiredAt = new Date().getTime() + parseInt(process.env.JWT_REFRESH_EXPIRATION);

    let token = uuidv4(); 

    let refreshTokenObj = new db.refreshToken({
        token: token,
        user: user._id,
        expiredDate: expiredAt.toString()
    });

    const refreshToken = await refreshTokenObj.save();

    return refreshToken.token;
}

module.exports = {
    createToken
}