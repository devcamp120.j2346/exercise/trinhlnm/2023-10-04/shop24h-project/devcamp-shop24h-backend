const expect = require("chai").expect;
const request = require("supertest");
const ProductType = require("../app/models/productType.model");
const app = require("../index");
const mongoose = require('mongoose');
const env = process.env.NODE_ENV || 'development';

describe("/api/v1/product-type", () => {
    before(async () => {
        await ProductType.deleteMany({});
    });

    after(async () => {
        mongoose.disconnect();
    });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h");
    });

    describe("GET /", () => {
        it("should return all product types", async () => {
            const productTypes = [
                { name: "name 1", description: "description 1" },
                { name: "name 2", description: "description 2" },
            ];
            const result = await ProductType.insertMany(productTypes);
            const res = await request(app).get("/api/v1/product-type");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(2);
        });
    });

    describe("GET/:id", () => {
        it("should return a ProductType if valid id is passed", async () => {
            const productType = new ProductType({
                name: "name 3",
                description: "description 3"
            });
            await productType.save();
            const res = await request(app).get("/api/v1/product-type/" + productType._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("name", productType.name);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/api/v1/product-type/adf");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/api/users/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });


    describe("POST /", () => {
        it("should return productType when the all request body is valid", async () => {
            const res = await request(app)
                .post("/api/v1/product-type")
                .send({
                    name: "name 4",
                    description: "description 4"
                });
            const data = res.body.data;
            expect(res.status).to.equal(201);
            expect(data).to.have.property("_id");
            expect(data).to.have.property("name", "name 4");
            expect(data).to.have.property("description", "description 4");

            const productType = await ProductType.findOne({ _id: data._id });
            expect(productType.name).to.equal('name 4');
            expect(productType.description).to.equal('description 4');
        });
    });

    describe("PUT /:id", () => {
        it("should update the existing productType and return 200", async () => {
            const productType = new ProductType({
                name: "name 5",
                description: "description 5"
            });
            await productType.save();

            const res = await request(app)
                .put("/api/v1/product-type/" + productType._id)
                .send({
                    name: "name 55",
                    description: "description 55"
                });

            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("name", "name 55");
            expect(res.body.data).to.have.property("description", "description 55");
        });
    });

    describe("DELETE /:id", () => {
        var id = "";
        const productType = new ProductType({
            name: "name 6",
            description: "description 6"
        });

        it("should delete requested id and return response 200", async () => {
            await productType.save();
            id = productType._id;
            const res = await request(app).delete("/api/v1/product-type/" + id);
            expect(res.status).to.be.equal(200);
        });

        it("should return 404 when deleted productType is requested", async () => {
            let res = await request(app).get("/api/v1/product-type/" + id);
            expect(res.status).to.be.equal(404);
        });
    });
});

